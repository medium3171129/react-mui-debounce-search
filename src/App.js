import { Grid, Paper, TextField, Typography } from "@mui/material";
import { debounce } from "lodash";
import React, { useEffect, useRef, useState } from "react";
import DebouncedAutocomplete from "./components/DebouncedAutocomplete";

function App() {
  const limit = 10;
  const [users, setUsers] = useState([]);
  let paginationRef = useRef({
    skip: 0,
    total: 0,
    hasMore: true,
  });

  const [searchQuery, setSearchQuery] = useState("");
  const [loading, setLoading] = useState(false);

  const fetchGetUserApi = async (api) => {
    setLoading(true);
    const response = await fetch(api);
    const json = await response.json();
    setLoading(false);
    return {
      list: json.users,
      pagination: {
        total: json.total,
        skip: json.skip + limit,
        limit: limit,
        hasMore: json.skip + limit < json.total,
      },
    };
  };

  useEffect(() => {
    (async () => {
      let api = `https://dummyjson.com/users/search?limit=${limit}&skip=0&q=${searchQuery}`;
      let { list, pagination: resPagination } = await fetchGetUserApi(api);
      const users = list.map((x) => ({
        ...x,
        label: x.email,
      }));

      setUsers(users);
      paginationRef.current = resPagination;
    })();
  }, [searchQuery]);
  console.log(paginationRef.current);

  return (
    <Grid
      container
      component="main"
      style={{
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundSize: "cover",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Grid
        item
        xs={12}
        sm={8}
        md={5}
        component={Paper}
        elevation={1}
        square
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          padding: 100,
        }}
      >
        <Typography component="h4" variant="secondary">
          Debounced Lazy Loading MUI Autocomplete Demo
        </Typography>
        <br /> <br />
        <div>
          <DebouncedAutocomplete
            id="debounced-autocomplete-demo"
            options={users}
            hasMore={paginationRef.current.hasMore}
            loading={loading}
            onFetchMore={async () => {
              if (!paginationRef.current.hasMore) return;
              setLoading(true);

              let api = `https://dummyjson.com/users/search?limit=${limit}&skip=${paginationRef.current.skip}&q=${searchQuery}`;

              let { list, pagination: resPagination } = await fetchGetUserApi(
                api
              );
              const users = list.map((x) => ({
                ...x,
                label: x.email,
              }));

              setUsers((prev) => [
                ...prev,
                ...users.filter(
                  (x) => !prev.map((u) => u.email).includes(x.email)
                ),
              ]);

              paginationRef.current = resPagination;

              setLoading(false);
            }}
            sx={{ width: 300 }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Search user"
                onChange={debounce((e) => {
                  setSearchQuery(e.target.value);
                }, 1000)}
                onBlur={debounce((e) => {
                  setSearchQuery(e.target.value);
                }, 1000)}
              />
            )}
          />
        </div>
      </Grid>
    </Grid>
  );
}

export default App;
