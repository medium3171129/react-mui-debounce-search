import LoadingButton from "@mui/lab/LoadingButton";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import React, { forwardRef, useCallback, useRef } from "react";

const AutocompleteRenderOptionComponent = forwardRef(
  ({ option, index, showLoadingBelow, isLastItem, hasMore, ...props }, ref) => {
    return (
      <>
        <Box ref={ref} {...props}>
          {option.image && (
            <Box
              component="img"
              sx={{
                height: 30,
                width: 50,
                maxHeight: { xs: 233, md: 167 },
                maxWidth: { xs: 350, md: 250 },
              }}
              alt={option.image}
              src={option.image}
            />
          )}
          {option.label}
        </Box>
        {isLastItem && showLoadingBelow && hasMore && (
          <>
            Load more <LoadingButton loading={true} />
          </>
        )}
           {isLastItem && !hasMore && (
          <>
            No more 🎉🎉🎉
          </>
        )}
      </>
    );
  }
);

export const DebouncedAutocomplete = ({
  options,
  loading,
  onFetchMore,
  hasMore,
  ...props
}) => {
  const observer = useRef();

  const lastOptionElementRef = useCallback(async (node) => {
    if (observer.current) observer.current.disconnect();
    observer.current = new IntersectionObserver(async (entries) => {
      if (entries[0].isIntersecting && hasMore) {
        await onFetchMore();
      }
    });
    if (node) observer.current.observe(node);
  }, []);

  return (
    <Autocomplete
      options={options}
      loading={loading}
      renderOption={(props, option, { index }) => {
        return (
          <AutocompleteRenderOptionComponent
            {...props}
            isLastItem={index === options.length - 1}
            showLoadingBelow={loading}
            option={option}
            hasMore={hasMore}
            ref={index === options.length - 1 ? lastOptionElementRef : null}
          />
        );
      }}
      {...props}
    />
  );
};

export default DebouncedAutocomplete;
